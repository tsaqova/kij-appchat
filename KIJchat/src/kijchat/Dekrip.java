/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kijchat;

import java.math.BigInteger;
import static kijchat.AESEnkrip.stringToHexString;
import static kijchat.Enkrip.plain;

/**
 *
 * @author reva
 */
public class Dekrip {
    static String key;
    static String cipher;
    public Dekrip(){
        
    }
    public void setCipher(String text){
        cipher = text;
        setKey();
    }
    private void setKey(){
        key = "1234123412341234";
    }
    int getDec(char c) {
        if(c!='A' && c!='B' && c!='C' && c!='D' && c!='E' && c!='F') {
            return c-'0';
        }
        else {
            return c-'A'+10;
        }
    }
    
    public String CFB(){
        int count = 0;
        byte[] keyByte = new byte[16];
        keyByte = hexStringToByteArray(stringToHexString(key));
        byte[] IV = new byte[16]; //16
        for(int j=0; j<16; j++) {
            IV[j] = (byte)0;
        }
        String res = "";
        AESEnkrip aes = new AESEnkrip();
        byte[] byteHexa = new byte[16];
        System.out.println(cipher + " " + cipher.length());
        for(int j=0; j<cipher.length(); j+=32) {
            for (int i=0; i<16; i++) {
                int hexa = 0;
                hexa = getDec(cipher.charAt(j+(i*2)))*16 + getDec(cipher.charAt(j+(i*2+1)));
                byteHexa[i] = (byte)hexa;
            }
            byte[] resEnkrip = new byte[16];
            resEnkrip = aes.enkrip(IV, keyByte);
            byte[] resXor = new byte[16];
            resXor = XOR(resEnkrip, byteHexa);
            res += new String(resXor);
            IV = byteHexa;
        }
        return res;
    }
    private byte[] XOR(byte[] a, byte[] b){
        for(int i=0; i<16; i++) {
            a[i] ^= b[i];
        }
        return a;
    }
    public static String stringToHexString(String string)
    {
        String res = String.format("%x", new BigInteger(1, string.getBytes()));
        return res;
    }
    public static byte[] hexStringToByteArray(String hexString)
    {
        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2)
        {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(
                    hexString.charAt(i + 1), 16));
        }
        return data;
    }
}
