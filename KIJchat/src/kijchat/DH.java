/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kijchat;

import java.math.BigInteger;
import java.util.Random;


/**
 *
 * @author Administrator
 */
public class DH {
        private static DH instance = null;
        public static  BigInteger privateKey;
	public static  BigInteger publicKey;
	public static  BigInteger secretKey;
        public static  BigInteger q;
        public static  BigInteger alpha;
	public static  int keySize;

	private DH() {
		keySize = 16;
                generateQA();
                generateKey(q, alpha);
	}
        
        public static DH getInstance() {
            if(instance == null) {
                instance = new DH();
            }
            return instance;
        }

	public static void print() {
		System.out.println("Public : " + getPublicKey());
		System.out.println("Private : " + getPrivateKey());
		System.out.println("Secret : " + getSecretKey());
	}
	
	public static  void generateSecretKey(BigInteger publicDestKey, BigInteger q) {
		BigInteger key = publicDestKey.modPow(getPrivateKey(), q);
		StringBuilder sb = new StringBuilder(getKeySize());
		String keyString = key.toString();
		int keyLength = keyString.length();
		for(int i=0; i<getKeySize(); i++) {
			char c = keyString.charAt(i%keyLength);
			sb.append(c);
		}
		BigInteger secretKe = new BigInteger(sb.toString());
		secretKey = secretKe;
                Enkrip.setKey(secretKey.toString());
	}
	
	public static  void generateKey(BigInteger q, BigInteger alpha) {
		generatePrivateKey(q, alpha);
		generatePublicKey(q, alpha);
	}
	
	public static  String getRandomNumber(int digit) {
		Random rand = new Random();
		StringBuilder sb = new StringBuilder(digit);
	    for(int i=0; i < digit; i++)
	        sb.append((char)('1' + rand.nextInt(9)));
	    return sb.toString();
	}
	
	public static  void generatePrivateKey(BigInteger q, BigInteger a) {
		BigInteger randomNumber = new BigInteger(getRandomNumber(keySize));
		privateKey = randomNumber;
	}

	public static  void generatePublicKey(BigInteger q, BigInteger a) {
		BigInteger key = a.modPow(privateKey, q);
		publicKey = key;
	}
	
	public static  BigInteger getPublicKey() {
		return publicKey;
	}

	public static  void setPublicKey(BigInteger publicKey) {
		publicKey = publicKey;
	}
	
	public static  BigInteger getPrivateKey() {
		return privateKey;
	}
	
	public static  void setPrivateKey(BigInteger privateKey) {
		privateKey = privateKey;
	}
	
	public static  int getKeySize() {
		return keySize;
	}

	public static  void setKeySize(int keySize) {
		keySize = keySize;
	}
	
	public static  BigInteger getSecretKey() {
		return secretKey;
	}

	public static  void setSecretKey(BigInteger secretKey) {
		secretKey = secretKey;
	}
        public static void generateQA() {
            q = BigInteger.valueOf(11059);
            alpha = BigInteger.valueOf(11054);
        }
}
