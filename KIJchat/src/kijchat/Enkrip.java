/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kijchat;

import java.math.BigInteger;

/**
 *
 * @author reva
 */
public class Enkrip {
    static String plain, key;
    static int count;
    public Enkrip(){
        key = "1234123412341234";
    }
    public static void setKey(String key){
        key = key;
    }
    public void setPlainAndKey(String text){
        plain = text;
    }
    public void checkSumByte(){
        int tempLength = plain.length();
        System.out.println(tempLength);
        if(tempLength % 16 != 0){
            tempLength %= 16;
            tempLength = 16 - tempLength;
            for(int i = 0; i < tempLength; i++){
                plain = plain.concat("\0");
            }
        }
        System.out.println(plain.length());
    }
    public String getKalimat(){
        return plain;
    }
    private static String getBlokKalimat(int idx){
        String blok = "";
        int batas = idx+16;
        for(int i = idx; i < batas; i++){
            blok += plain.charAt(i);
        }
        return blok;
    }
    public String CFB(){
        int i = 0, batas;
        count = 0;
        batas = plain.length();
        byte[] IV = new byte[16]; //16
        for(int j=0; j<16; j++) {
            IV[j] = (byte)0;
        }
        byte[] chip = new byte[16];
        String blok = "";
        byte[] chipper = new byte[16];
        byte[] keyByte = hexStringToByteArray(stringToHexString(key));
        AESEnkrip aes = new AESEnkrip();
        String res = "";
        while(i < batas){
            blok = getBlokKalimat(i);
            byte[] blokByte = new byte[16];
            blokByte = hexStringToByteArray(stringToHexString(blok));
            i += 16;
            chip = aes.enkrip(IV, keyByte);
            chipper = XOR(chip, blokByte);
            IV = chipper;
            for(int j=0; j<16; j++) {
                res += String.format("%02X", chipper[j]);
            }
        }
        return res;
    }
    private byte[] XOR(byte[] a, byte[] b){
        for(int i=0; i<16; i++) {
            a[i] ^= b[i];
        }
        return a;
    }
    public static void main(String[] args) {
        /*
        Enkrip enk = new Enkrip();
        enk.checkSumByte();
        String res = "";
        res = enk.CFB();
        System.out.println("!"+res+"!" + res.length());
        Dekrip dek = new Dekrip(res, "1234123412341234");
        System.out.println(dek.CFB());
        */
    }
    
    private static byte[][] arrayToMatrix(byte[] array)
    {
        byte[][] matrix = new byte[4][4];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                matrix[j][i] = array[i * 4 + j];
            }
        }
        return matrix;
    }

    private static byte[] matrixToArray(byte[][] matrix)
    {
        byte[] array = new byte[16];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                array[i * 4 + j] = matrix[j][i];
            }
        }
        return array;
    }

    public static String stringToHexString(String string)
    {
        String res = String.format("%x", new BigInteger(1, string.getBytes()));
        return res;
    }

    public static String byteArrayToHexString(byte[] hexArray)
    {
        String hexString = new String();
        for (byte hex : hexArray)
        {
            hexString += Integer.toString(hex & 0xFF, 16);
        }

        return hexString;
    }
    public static byte[] hexStringToByteArray(String hexString)
    {
        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2)
        {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(
                    hexString.charAt(i + 1), 16));
        }
        return data;
    }
    
    private static byte[][] addRoundKey(byte[][] state, byte[][] key)
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                state[j][i] ^= key[j][i];
            }
        }
        return state;
    }
}