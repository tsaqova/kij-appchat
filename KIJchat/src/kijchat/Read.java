/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kijchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reva
 */
public class Read implements Runnable {
    private Thread t;
    //private String threadName;
    BufferedReader br;
    StringBuffer sb = new StringBuffer();
    String response;
    KIJ_CHAT_UI chat;
    Dekrip dekrip = new Dekrip();
    
    Read(BufferedReader br, KIJ_CHAT_UI chat){
        //threadName = name;
        this.br = br;
        this.chat = chat;
        //System.out.println("Creating " +  threadName );
    }
    public void run() {
        //dekrip = new Dekrip();
        while(true){
            try {
                do {
                    char[] c = new char[] { 1024 };
                    br.read(c);
                    sb.append(c);
                }while (br.ready());
                response = sb.toString();
                sb.delete(0, sb.length());
                //chat.appendTextAREA(response);
                System.out.println("\nSERVER> !" + response + "!");

                int count = 1;
                String temp[] = new String[100];
                
                //System.out.println("pesan yang dipecah: ");
                
                for (String retval: response.split(" ", 3)){
                    temp[count] = retval;
                    //System.out.println(count + " " + retval);
                    count++;
                }
                //System.out.println("--end pesan yang dipecah");
                if(temp[1].equals("CONNECT")){
                    
                } else if(temp[1].equals("DISCONNECT")){
                    
                } else if(temp[1].equals("LIST")){
                    if(temp[2].equals("OK")){
                        for (String retval: temp[3].split(";")){
                            //System.out.println("orang " + retval);
                            this.chat.appendTextONLINE(retval + "\n");
                            //chat.appendTextAREA(retval + "\n");
                        }
                    } else {
                        chat.appendTextAREA("Koneksi gagal\n");
                    }
                } else {
                    if(temp[1].charAt(0) == '1') {
                        DH.getInstance().q = new BigInteger(temp[3]);
                    }
                    else if(temp[1].charAt(0) == '2') {
                        DH.getInstance().alpha = new BigInteger(temp[3]);
                        chat.sendDH2(DH.getPublicKey(), temp[1].substring(1));
                    }
                    else if(temp[1].charAt(0) == '3') {
                        DH.generateSecretKey(new BigInteger(temp[3]), DH.q);
                        System.out.println(DH.secretKey);
                        chat.sendDH3(DH.getPublicKey(), temp[1].substring(1));
                    }
                    else if(temp[1].charAt(0) == '4') {
                        DH.generateSecretKey(new BigInteger(temp[3]), DH.q);
                        System.out.println(DH.secretKey);
                        chat.sendDH4(temp[1].substring(1));
                    }
                    else if(temp[1].charAt(0) == '5') {
                        chat.sendChat2();
                    }
                    else {
                        if(temp[2].equals("OK")){
                            chat.appendTextAREA("Pesan terkirim\n");
                        } else if(temp[2].equals("NO")) {
                            chat.appendTextAREA("Username " + temp[3] + " tidak ada.\n");
                        } else {
                            System.out.println("YANG DITERIMA (sebelum didekrip): " + temp[1] + temp[2] + temp[3]);
                            dekrip.setCipher(temp[3]);
                            temp[3] = dekrip.CFB();
                            System.out.println("YANG DITERIMA (setelah didekrip): " + temp[3]);
                            chat.appendTextAREA(temp[2] + "> " + temp[3] + ".\n");
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
   
    public void start ()
    {
       //System.out.println("Starting " +  threadName );
          t = new Thread (this);
          t.start ();
    }
}
