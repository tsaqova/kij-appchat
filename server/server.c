#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "command.h"

node *root = NULL;

void argument_check(int argc, char *argv[]) {
  if(argc < 2) {
    printf("error, no port provided\n");
    exit(1);
  }
}

void *connection_handler(void *_sockfd)
{
  int sockfd = *(int*)_sockfd;
  int msg_in_length;

  char msg_out[BUFFER_SIZE], msg_in[BUFFER_SIZE];
  char arg0[BUFFER_SIZE], arg1[BUFFER_SIZE], arg2[BUFFER_SIZE];
  bzero(msg_in, BUFFER_SIZE);
  bzero(msg_out, BUFFER_SIZE);

  while((msg_in_length = recv(sockfd, msg_in, BUFFER_SIZE, 0)) > 0)
  {
    if(msg_in_length < 0) {
      printError("error reading from socket");
    }
    bzero(arg0, sizeof(arg0));
    bzero(arg0, sizeof(arg1));
    bzero(arg0, sizeof(arg2));
    sscanf(msg_in, "%s %s %[^\n]", arg0, arg1, arg2);
    printf(".%s. .%s. .%s.\n", arg0, arg1, arg2);
    runCommand(sockfd, &root, arg0, arg1, arg2);
    bzero(msg_in, sizeof(msg_in));
  }
  close(sockfd);

  if(msg_in_length==0)
  {
    fprintf(stderr, "Client disconnected\n");
  }
  else
  {
    fprintf(stderr, "Recv Failed\n");
  }
  return 0;
}

void startServer(int port) {
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    printError("error opening socket\n");
  }
  struct sockaddr_in server_addr;
  bzero((char *)&server_addr, sizeof(server_addr));

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  server_addr.sin_addr.s_addr = INADDR_ANY;

  int ret = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
  if(ret < 0) {
    printError("error on binding\n");
  }
  listen(sockfd, 5);

  struct sockaddr_in client_addr;
  int client_length = sizeof(client_addr);
  int newsockfd;
  while(newsockfd = accept(sockfd, (struct sockaddr *) &client_addr, &client_length)) {
    if(DEBUG) printf("Nomor socket : %d\n", newsockfd);
    if(newsockfd < 0) {
      printError("error on accept");
    }

    fprintf(stderr, "Connection accepted\n");
    pthread_t thread;
    int *pnewsockfd;
    pnewsockfd = malloc(sizeof(newsockfd));
    *pnewsockfd = newsockfd;

    if(pthread_create(&thread, NULL, connection_handler, (void *) pnewsockfd) < 0) {
      printError("Could not create thread");
    }

    fprintf(stderr, "Handler assigned\n");
  }
}

int main(int argc, char *argv[]) {

  argument_check(argc, argv);

  int port = atoi(argv[1]);

  startServer(port);

  return 0;

}
