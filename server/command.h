#ifndef COMMAND_H
#define COMMAND_H
#include "avltree.h"

#define DEBUG 1

void printError(char *msg)
{
  perror(msg);
  exit(1);
}

void sendCommand(int sockfd, char msg_out[BUFFER_SIZE]) {
  int msg_out_length = send(sockfd, msg_out, strlen(msg_out), 0);
  if(DEBUG) printf("msg_out %s length %d\n", msg_out, msg_out_length);
  if(msg_out_length < 0) {
    if(DEBUG) printError("error writing to socket");
  }
}

void connectChat(int sockfd, node **now, char username[BUFFER_SIZE]) {
  int found = searchNode(&(*now), username);
  char msg_out[BUFFER_SIZE];
  bzero(msg_out, sizeof(msg_out));
  if(DEBUG) printf("found %d\n", found);
  if(found) {
    strcpy(msg_out, "CONNECT NO -");
    sendCommand(sockfd, msg_out);
  }
  else {
    insertNode(&(*now), username, sockfd);
    strcpy(msg_out, "CONNECT OK -");
    sendCommand(sockfd, msg_out);
  }
}

void disconnectChat(int sockfd, node **now, char username[BUFFER_SIZE]) {
  int found = searchNode(&(*now), username);
  if(DEBUG) printf("found %d\n", found);
  if(found) {
    removenode(&(*now), username);
  }
  else {
    insertNode(&(*now), username, sockfd);
    sendCommand(sockfd, "DISCONNECT NO -");
  }
}

void listConnectedUser(int sockfd, node **now) {
  char *msg_out;
  msg_out = (char *)malloc(BUFFER_SIZE);
  strcpy(msg_out, "LIST OK ");
  strcat(msg_out, getAllUsers(&(*now)));
  msg_out[strlen(msg_out)-1] = '\0';
  if(DEBUG) printf("%s\n", msg_out);
  sendCommand(sockfd, msg_out);
}

void sendChat(int sockfd, node **now, char from[BUFFER_SIZE], char to[BUFFER_SIZE], char message[BUFFER_SIZE]) {
  if(searchNode(&(*now), to)) {
    int sock_receiver = getSocketfd(&(*now), to);
    char *msg_out_sender;
    msg_out_sender = (char *)malloc(BUFFER_SIZE);
    strcpy(msg_out_sender, "SEND OK -");
    if(DEBUG) printf("ke %s %s\n", from, msg_out_sender);
    printf("%d\n", searchNode(&(*now), from));
    int sock_sender = getSocketfd(&(*now), from);
    sendCommand(sock_sender, msg_out_sender);

    char *msg_out_receiver;
    msg_out_receiver = (char *)malloc(BUFFER_SIZE);
    bzero(msg_out_receiver, sizeof(msg_out_receiver));
    strcpy(msg_out_receiver, "SEND ");
    strcat(msg_out_receiver, from);
    strcat(msg_out_receiver, " ");
    strcat(msg_out_receiver, message);
    if(DEBUG) printf("ke %s %s\n", to, msg_out_receiver);
    printf(".%s.\n", to);
    printf("%d\n", searchNode(&(*now), to));
    sendCommand(sock_receiver, msg_out_receiver);
  }
  else {
    char *msg_out_sender;
    msg_out_sender = (char *)malloc(BUFFER_SIZE);
    strcpy(msg_out_sender, "SEND NO ");
    strcat(msg_out_sender, to);
    if(DEBUG) printf("%s\n", msg_out_sender);
    int sock_sender = getSocketfd(&(*now), from);
    sendCommand(sock_sender, msg_out_sender);
  }
}

int cek(char c) {
  if((c>=48 && c<=57) || (c>=65 && c<=90) || (c>=97 && c<=122)) {
    return 0;
  }
  else {
    return 1;
  }
}

void teruskan(int sockfd, node **now, char arg0[BUFFER_SIZE], char arg1[BUFFER_SIZE], char arg2[BUFFER_SIZE]) {
  if(searchNode(&(*now), arg1)) {
    int sock_receiver = getSocketfd(&(*now), arg1);
    char *msg_out_receiver;
    msg_out_receiver = (char *)malloc(BUFFER_SIZE);
    bzero(msg_out_receiver, sizeof(msg_out_receiver));
    strcpy(msg_out_receiver, arg0);
    strcat(msg_out_receiver, " ");
    strcat(msg_out_receiver, arg1);
    strcat(msg_out_receiver, " ");
    strcat(msg_out_receiver, arg2);
    if(DEBUG) printf("ke %s %s\n", arg1, msg_out_receiver);
    printf(".%s.\n", arg2);
    printf("%d\n", searchNode(&(*now), arg1));
    sendCommand(sock_receiver, msg_out_receiver);
  }
}

void runCommand(int sockfd, node **root, char arg0[BUFFER_SIZE], char arg1[BUFFER_SIZE], char arg2[BUFFER_SIZE]) {
  if(strcmp(arg0, "CONNECT") == 0) {
    connectChat(sockfd, &(*root), arg1);
  }
  else if(strcmp(arg0, "DISCONNECT") == 0) {
    disconnectChat(sockfd, &(*root), arg1);
  }
  else if(strcmp(arg0, "LIST") == 0) {
    listConnectedUser(sockfd, &(*root));
  }
  else if(arg0[0] == '1') {
    teruskan(sockfd, &(*root), arg0, arg1, arg2);
  }
  else if(arg0[0] == '2') {
    teruskan(sockfd, &(*root), arg0, arg1, arg2);
  }
  else if(arg0[0] == '3') {
    teruskan(sockfd, &(*root), arg0, arg1, arg2);
  }
  else if(arg0[0] == '4') {
    teruskan(sockfd, &(*root), arg0, arg1, arg2);
  }
  else if(arg0[0] == '5') {
    teruskan(sockfd, &(*root), arg0, arg1, arg2);
  }
  else {
    sendChat(sockfd, &(*root), arg0, arg1, arg2);
  }
}

#endif