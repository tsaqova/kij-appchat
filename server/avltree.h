#ifndef AVLTREE_H
#define AVLTREE_H
/*
    Created by : Yusro Tsaqova
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFER_SIZE 128

char listUsers[BUFFER_SIZE];

typedef struct _node {
    int sockfd;
    char username[BUFFER_SIZE];
    struct _node *left, *right;
} node;

int height(node **now){
    int h=0;
    if(*now){
       int l_height = height(&((*now)->left));
       int r_height = height(&((*now)->right));
       int maks = 0;
       if(l_height > r_height) {
        maks = l_height;
       }
       else {
        maks = r_height;
       }
       h = maks+1;
    }
    return h;
}

int diff(node **now){
    if(*now == NULL) return 0;
    int l_height = height(&((*now)->left));
    int r_height = height(&((*now)->right));
    return l_height - r_height;
}

void ll_rotate(node **now){
    if(*now) {
        node* temp = (*now)->right;
        (*now)->right = temp->left;
        temp->left = *now;
        *now = temp;
    }
}

void rr_rotate(node **now){
    if(*now) {
        node* temp = (*now)->left;
        (*now)->left = temp->right;
        temp->right = (*now);
        *now = temp;
    }
}

void lr_rotate(node **now){
    if(*now) {
        rr_rotate(&((*now)->left));
        ll_rotate(&(*now));
    }
}

void rl_rotate(node **now){
    if(*now) {
        rr_rotate(&((*now)->right));
        ll_rotate(&(*now));
    }
}

void balance(node **now){
    if(*now) {
        int bal_fact = diff(&(*now));
        if(bal_fact>1){
            if(diff(&((*now)->left))>0){
                rr_rotate(&(*now));
            }
            else{
                lr_rotate(&(*now));
            }
        }
        else if(bal_fact<-1){
            if(diff(&((*now)->right))>0){
                rl_rotate(&(*now));
            }
            else{
                ll_rotate(&(*now));
            }
        }
    }
}

void insertNode(node** now, char username[BUFFER_SIZE], int sockfd){
    if(*now == NULL) {
        node *newnode;
        newnode = (node *)malloc(sizeof(node));
        newnode->left = newnode->right = NULL;
        strcpy(newnode->username, username);
        newnode->sockfd = sockfd;
        *now = newnode;
    }
    else if(strcmp(username, (*now)->username) < 0) {
        insertNode(&((*now)->left), username, sockfd);
    }
    else if(strcmp(username, (*now)->username) > 0){
        insertNode(&((*now)->right), username, sockfd);
    }
    balance(&(*now));
}

int searchNode(node** now, char username[BUFFER_SIZE]){
    if(*now == NULL) return 0;
    else if(strcmp(username, (*now)->username) == 0) {
        return 1;
    }
    else if(strcmp(username, (*now)->username) < 0) {
        searchNode(&((*now)->left), username);
    }
    else if(strcmp(username, (*now)->username) > 0) {
        searchNode(&((*now)->right), username);
    }
}

int removenode(node** now, char username[BUFFER_SIZE]){
    if(*now == NULL) return -1;
    else if(strcmp(username, (*now)->username) < 0){
        removenode(&((*now)->left), username);
    }
    else if(strcmp(username, (*now)->username) > 0){
        removenode(&((*now)->right), username);
    }
    else{
        if(((*now)->left) && ((*now)->right)){
            node *temp;
            temp=(*now)->left;
            while(temp->right)temp=temp->right;

            strcpy((*now)->username, temp->username);

            removenode(&((*now)->left), temp->username);
        }
        else{
            node* temp = (*now);
            (*now) = ((*now)->left) ? (*now)->left : (*now)->right;
            free(temp);
        }
        return 1;
    }
}

int getSocketfd(node** now, char username[BUFFER_SIZE]){
    if(*now == NULL) return 0;
    else if(strcmp(username, (*now)->username) == 0) {
        return (*now)->sockfd;
    }
    else if(strcmp(username, (*now)->username) < 0) {
        getSocketfd(&((*now)->left), username);
    }
    else if(strcmp(username, (*now)->username) > 0) {
        getSocketfd(&((*now)->right), username);
    }
}

void traverse(node **now) {
    if(*now == NULL) {
        return;
    }
    traverse(&((*now)->left));
    strcat(listUsers, (*now)->username);
    strcat(listUsers, ";");
    traverse(&((*now)->right));
}

char *getAllUsers(node **now) {
    bzero(listUsers, sizeof(listUsers));
    traverse(&(*now));
    return listUsers;
}

void print(node** now){
    if(*now == NULL) {
        puts("null");
        return;
    }
    puts("left");
    print(&((*now)->left));
    printf("%s %d\n", (*now)->username, (*now)->sockfd);
    puts("right");
    print(&((*now)->right));
}

#endif


